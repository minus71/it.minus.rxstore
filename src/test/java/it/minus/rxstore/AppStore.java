package it.minus.rxstore;

import java.math.BigDecimal;
import java.util.List;

public class AppStore extends AbstractStore {


	public static final KEY<String>       K_APP_NAME = key();
	public static final KEY<Integer>      K_POLLING_MS= key();
	public static final KEY<List<String>> K_USERS= key();
	public static final KEY<BigDecimal>   RATING= key();
	
	private  static final <T> KEY<T> key() {
		return key(AppStore.class);
	}
	
	
	
	
	
	
}
