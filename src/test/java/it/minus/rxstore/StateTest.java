package it.minus.rxstore;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class StateTest {

	State state;
	
	@Before
	public void setUp() throws Exception {
		state= State.builder()
			.with(AppStore.K_APP_NAME, "Foo")
			.with(AppStore.K_POLLING_MS, 12)
			.build();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGet() {
		
		Integer integer = state.get(AppStore.K_POLLING_MS).get();
		Integer expected = new Integer(12);
		assertEquals(expected, integer);

		String str = state.get(AppStore.K_APP_NAME).get();
		assertEquals("Foo", str);

		
		Optional<BigDecimal> opt = state.get(AppStore.RATING);
		assertFalse(opt.isPresent());
		
		try {
			opt.get();
			fail("Optional get of empty should throw!");
		}catch (NoSuchElementException e) {
			// OK!
		}
	}

	@Test
	public void testOption() {
		State option = state.option(AppStore.K_APP_NAME,AppStore.RATING);
		assertEquals(1, option.size());
		String appName = option.get(AppStore.K_APP_NAME).get();
		assertEquals("Foo", appName);
	}
	
	@Test
	public void testApplicationEmpty() {
		Optional<State> application = state.application(AppStore.K_APP_NAME,AppStore.RATING);
		assertFalse(application.isPresent());
	}

	@Test
	public void testApplicationPresent() {
		Optional<State> application = state.application(AppStore.K_APP_NAME,AppStore.K_POLLING_MS);
		assertTrue(application.isPresent());
		
		Integer integer = application.get().get(AppStore.K_POLLING_MS).get();
		Integer expected = new Integer(12);
		assertEquals(expected, integer);

		String str = application.get().get(AppStore.K_APP_NAME).get();
		assertEquals("Foo", str);
	}
	

	
	final KEY<Integer> KEY_INT = new KEY<Integer>() {
		@Override
		public String toString() {
			return "KEY_INT";
		}
	}; 
	final KEY<String> KEY_STRING = new KEY<String>() {
		@Override
		public String toString() {
			return "KEY_STRING";
		}
	}; 
	
	@Test 
	public void getSetWithEnumKeys() {
		State state2 = State.builder()
			.with(KEY_STRING, "Foo")
			.with(KEY_INT, 12)
			.build();
		
		Integer intVal = state2.get(KEY_INT).get();
		assertEquals(new Integer(12), intVal);

		String strVal = state2.get(KEY_STRING).get();
		assertEquals("Foo", strVal);
	}
	@Test 
	public void stateToString() {
		State state2 = State.builder()
			.with(KEY_STRING, "Foo")
			.with(KEY_INT, 12)
			.build();

		assertEquals("State:{{KEY_STRING=Foo, KEY_INT=12}}", state2.toString());
	}
	
}
