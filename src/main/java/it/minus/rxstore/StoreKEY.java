package it.minus.rxstore;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;


class StoreKEY<T> implements KEY<T> {

	public final int id;
	private String name;
	Class<State> owner;

	StoreKEY(int id) {
		this.id = id;
	}

	StoreKEY(Class<State> cls, int id) {
		this.id = id;
		this.owner =cls;
	}


	@Override
	public String toString() {
		lazyGet();
		return name;
	}

	private void lazyGet() {
		if (name == null) {
			name = "KEY@" + id;
			Field[] fields = owner.getFields();
			for (Field field : fields) {
				int modifiers = field.getModifiers();
				if (Modifier.isStatic(modifiers) && Modifier.isPublic(modifiers)) {
					Object value;
					try {
						value = field.get(null);
						if (value != null && value instanceof KEY && value.equals(this)) {
							name = field.getName();
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

			}
		}
	}

}
