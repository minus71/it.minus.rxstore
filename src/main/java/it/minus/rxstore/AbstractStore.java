package it.minus.rxstore;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;

public abstract class AbstractStore implements Store {

	private static AtomicInteger keyCounter = new AtomicInteger(0);
	
	private List<StoreReducer> reducers = new ArrayList<>();
	
	private State state = State.builder().build();
	
	private BehaviorSubject<State> subject = BehaviorSubject.createDefault(state);
	
	
	
	public void dispatch(StoreAction action) {
		state = reducers.stream().reduce(state,
				(ks,r)->r.reduce(ks, action),
				(o,n)->n);
		subject.onNext(state);
	}
	
	@Override
	public void addReducer(StoreReducer reducer) {
		ArrayList<StoreReducer> next = new ArrayList<>(reducers);
		next.add(reducer);
		reducers= next;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> Observable<T> select(KEY<T> key) {
		return subject
				.doOnNext(z->System.out.println("Select:"+z))
				.map(s->s.get(key)).filter(Optional::isPresent)
				.doOnNext(System.out::println)
				.map(Optional::get).distinctUntilChanged();
	}
	
	protected static <T> KEY<T> key(Class cls){
		int id = keyCounter.incrementAndGet();
		StoreKEY<T> k = new StoreKEY<T>(cls, id);
		return k;
	}
	
}
