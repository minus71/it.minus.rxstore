package it.minus.rxstore;

import static it.minus.rxstore.AppStore.*;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.disposables.Disposable;

public class AppStoreTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		KEY<String> kAppName = K_APP_NAME;
		
		assertEquals("K_APP_NAME", kAppName.toString());
	}

	
	@Test
	public void testAction() {
		
		AppStore store = new AppStore();
		store.addReducer(new AppReducer());
		
		
		Observable<String> select = store.select(K_APP_NAME);

		List<String> appSet = new ArrayList<>();
		store.dispatch(new SetAppName("BOOMS"));
		store.dispatch(new SetAppName("FOO"));
		Disposable sub = select.subscribe(s->{
			appSet.add(s);
		});
		store.dispatch(new SetAppName("BAR"));
		store.dispatch(new AddUser("MINUS"));
		store.dispatch(new AddUser("ALE"));
		store.dispatch(new AddUser("AURE"));
		store.dispatch(new AddUser("ALE"));

		store.dispatch(new SetAppName("BAR"));
		store.dispatch(new SetAppName("BAZ"));
		
		sub.dispose();
		store.select(K_USERS).subscribe(u->System.out.println(u));
		store.dispatch(new AddUser("LUIS"));
		
		assertEquals(Arrays.asList("FOO","BAR","BAZ"), appSet);
		
	}

}
