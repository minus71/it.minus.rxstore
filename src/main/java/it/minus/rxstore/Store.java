package it.minus.rxstore;

import io.reactivex.Observable;

public interface Store {
	void dispatch(StoreAction action);
	<T> Observable<T> select(KEY<T> key);
	void addReducer(StoreReducer reducer);
}
