package it.minus.rxstore;

public class SetAppName implements StoreAction {
	public final String name;

	public SetAppName(String name) {
		super();
		this.name = name;
	}
	
}
