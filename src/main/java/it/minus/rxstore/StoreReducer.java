package it.minus.rxstore;

public interface StoreReducer {

	State reduce(State state, StoreAction action);
}
