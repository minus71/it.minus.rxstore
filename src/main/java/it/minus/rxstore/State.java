package it.minus.rxstore;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

public class State {

	
	@SuppressWarnings("rawtypes")
	private Map<KEY, Object> keyValueMap=new HashMap<>();
	
	private State() {
	}

	@SuppressWarnings("unchecked")
	public <T> Optional<T> get(KEY<T> key) {
		return (Optional<T>) Optional.ofNullable(keyValueMap.get(key));
	}
	
	public static Builder builder() {
		return new Builder();
	}
	
	static class Builder {
		@SuppressWarnings("rawtypes")
		private final Map<KEY, Object> keys = new LinkedHashMap<>();
		public <T> Builder with(KEY<T> k, T value) {
			keys.put(k, value);
			return this;
		}
		public State build(){
			State keySet = new State();
			keySet.keyValueMap = keys;
			return keySet;
		}
	}
	
	public int size() {
		return keyValueMap.size();
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public State option(KEY ...keys) {
		Builder builder = builder();
		for (KEY key : keys) {
			Object v = keyValueMap.get(key);
			if(v!=null) {
				builder.with(key, v);
			}
		}
		return builder.build();
	}
	
	public static <T> State just(KEY<T> k, T v) {
		State state = new State();
		state.keyValueMap.put(k, v);
		return state;
	}
	
	@SuppressWarnings("rawtypes")
	public Optional<State> application(KEY ...keys) {
		State option = option(keys);
		return option.size() == keys.length ? Optional.of(option):Optional.empty();
	}

	public State mergeWith(State that){
		State result = new State();
		result.keyValueMap.putAll(this.keyValueMap);
		result.keyValueMap.putAll(that.keyValueMap);
		return result;
	}
	
	public <T> State update(KEY<T> k, T v) {
		return mergeWith(just(k, v));
	}

	@Override
	public String toString() {
		
		return "State:{"+keyValueMap.toString()+"}";
	}
	
	

	
}
