package it.minus.rxstore;

import static it.minus.rxstore.AppStore.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
public class AppReducer implements StoreReducer{

	@Override
	public State reduce(State state, StoreAction action) {
		if(action instanceof SetAppName) {
			return state.update(K_APP_NAME,((SetAppName) action).name);
		} else if(action instanceof AddUser) {
			String username = ((AddUser) action).user;
			
			Optional<List<String>> users = state.get(K_USERS);
			List<String> otherUsers = users.orElse(new ArrayList<>());
			List<String> nextUsers;
			if(otherUsers.contains(username)) {
				nextUsers = otherUsers;
			}else {
				nextUsers = new ArrayList<>(otherUsers);
				nextUsers.add(username);
			}
			return state.update(K_USERS, nextUsers);
			
		} else {
			return state;
		}
	}

}
